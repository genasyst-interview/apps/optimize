<?php

class optimizeBackendAction extends waViewAction
{
    public function execute()
    {
        if (waRequest::method() == 'post') {
            $this->_run(waRequest::post('files'));
        } else {
            $apps = waRequest::get('apps', array('site' => array('' => '')), waRequest::TYPE_ARRAY);
            $file_statuses = waRequest::get('view_files', array(), waRequest::TYPE_ARRAY);
            $file_types = waRequest::get('file_types', array(), waRequest::TYPE_ARRAY);
            $settings = array();
            if (!empty($file_statuses)) {
                $settings['file_statuses'] = $file_statuses;
            }
            if (!empty($file_types)) {
                $settings['file_types'] = $file_types;

            }
            if (!empty($settings)) {
                optimizeFiles::setListDirSetings($settings);
            }
            $files = array();
            foreach ($apps as $app => $dirs) {
                foreach ($dirs as $path) {
                    $files[$app][$path] = optimizeFiles::listdir(optimizeFiles::getDataPath($app, $path));
                }
            }
            $menu_apps = array(
                'site' => array(
                    'name' => 'Приложение сайт',
                    'dirs' => array('' => 'Все'),
                ),
                'shop' =>
                    array(
                        'name' => 'Приложение Магазин',
                        'dirs' => array(
                            ''          => 'Все',
                            'products'  => 'Продукты',
                            '^products' => 'Все исключая продукты',
                        ),
                    ),
                'blog' => array(
                    'name' => 'Приложение Блог',
                    'dirs' => array('' => 'Все'),
                ),
            );

            $this->view->assign('files', $files);
            $this->view->assign('apps', $menu_apps);
        }
    }

    public function _run($files)
    {
        foreach ($files as $id) {
            $file = new optimizeFile($id);
            $file->optimize();
        }
    }
}
