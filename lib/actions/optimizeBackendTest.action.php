<?php

class optimizeBackendTestAction extends waViewAction
{
    protected $files = array(

        'jpg' => array(
            'test1' => 'wa-apps/optimize/img/tests/test1.jpg',
            'test2' => 'wa-apps/optimize/img/tests/test2.jpg',
        ),
        'png' => array(
            'test1' => 'wa-apps/optimize/img/tests/test1.png',
            'test2' => 'wa-apps/optimize/img/tests/test2.png',
        ),
        'gif' => array(
            'test1' => 'wa-apps/optimize/img/tests/test1.gif',
            'test2' => 'wa-apps/optimize/img/tests/test2.gif',
        ),
    );

    public function execute()
    {
        if (waRequest::method() == 'post') {
            $this->refreshFiles();

            $quality = null;
            $_q = waRequest::post('quality', 0, waRequest::TYPE_INT);
            if ($_q > 0) {
                $quality = $_q;
            }
            $_files_ = $this->getFiles();
            foreach ($_files_ as $type => $files) {
                foreach ($files as $id => $file) {
                    $file->optimize($quality, true);
                }
            }
            $this->view->assign('files', $_files_);
            $this->view->assign('quality', $quality);
        } else {
            $this->refreshFiles();
            $this->view->assign('files', $this->getFiles());
        }

    }

    protected function getFiles()
    {
        $files = array();
        $files['gif']['test1'] = new optimizeFile('wa-apps/optimize/img/tests/test1.gif');
        $files['gif']['test2'] = new optimizeFile('wa-apps/optimize/img/tests/test2.gif');

        $files['jpg']['test1'] = new optimizeFile('wa-apps/optimize/img/tests/test1.jpg');
        $files['jpg']['test2'] = new optimizeFile('wa-apps/optimize/img/tests/test2.jpg');

        $files['png']['test1'] = new optimizeFile('wa-apps/optimize/img/tests/test1.png');
        $files['png']['test2'] = new optimizeFile('wa-apps/optimize/img/tests/test2.png');

        return $files;
    }

    protected function refreshFiles()
    {
        $model = new optimizeFileModel();
        foreach ($this->getFiles() as $type => $files) {
            foreach ($files as $id => $file) {
                $original = optimizeFiles::getRootPath() . 'wa-apps/optimize/img/tests/' . $id . '.original.' . $type;
                $filepath = optimizeFiles::getRootPath() . $file;
                waFiles::copy($original, $filepath);
                $f_data = $model->getByLink($filepath);
                if (!empty($f_data) && is_array($f_data)) {
                    $model->deleteById($f_data['id']);
                }
            }
        }
    }

}
