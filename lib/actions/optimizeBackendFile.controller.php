<?php

class optimizeBackendFileController extends waJsonController
{
    public function execute()
    {
        if (waRequest::method() == 'post') {
            $id = waRequest::post('id');
            $_reoptimize = waRequest::post('reoptimize', 0, waRequest::TYPE_INT);
            $reoptimize = false;
            if ($_reoptimize > 0) {
                $reoptimize = true;
            }
            $quality = null;
            $_q = waRequest::post('quality', 0, waRequest::TYPE_INT);
            if ($_q > 0) {
                $quality = $_q;
            }
            $file = new optimizeFile($id);
            $file->optimize($quality, $reoptimize);
            clearstatcache();
            $this->response = array(
                'id'                => $file->getId(),
                'filepath'          => $file->getFilePath(),
                'size'              => $file->getFormatSize(),
                'original_filepath' => $file->getOriginalFilePath(),
                'original_filesize' => $file->getOriginalFormatSize(),
            );

        } else {
            $this->response = array(444 => 555);
        }
    }
}
