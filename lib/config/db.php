<?php
return array(
    'optimize_file' => array(
        'id'                => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'original_filepath' => array('varchar', 255, 'null' => 0),
        'filepath'          => array('varchar', 255, 'null' => 0),
        'filename'          => array('varchar', 255, 'null' => 0),
        'ext'               => array('varchar', 50, 'null' => 0),
        'optimize'          => array('tinyint', 1, 'null' => 0, 'default' => '0'),
        'app'               => array('varchar', 50, 'null' => 0),
        'md5'               => array('varchar', 50, 'null' => 0),
        'create_datetime'   => array('datetime', 'null' => 0),
        ':keys'             => array(
            'PRIMARY' => 'id',
        ),
    ),
);