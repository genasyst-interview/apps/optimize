<?php

class optimizeImageFactory
{
    const OPTIMIZER_SMART = 'smart';

    private $optimizers = array();
    private $options = array(
        'ignore_errors'     => true,
        'optipng_options'   => array('-i0', '--force', 'quality' => '-o2', '-quiet'),
        'pngquant_options'  => array('--force'),
        'pngcrush_options'  => array('-reduce', '-q', '-ow'),
        'pngout_options'    => array('-s3', '-q', '-y'),
        'gifsicle_options'  => array('-b', '-O5'),
        'jpegoptim_options' => array('--strip-all', '--all-progressive', 'quality' => '-m 90'),
        'jpegtran_options'  => array('-optimize', '-progressive'),
        'advpng_options'    => array('-z', '-4', '-q'),
        'optipng_bin'       => 'optipng',
        'pngquant_bin'      => 'pngquant',
        'pngcrush_bin'      => 'pngcrush',
        'pngout_bin'        => 'pngout',
        'gifsicle_bin'      => 'gifsicle',
        'jpegoptim_bin'     => 'jpegoptim',
        'jpegtran_bin'      => 'jpegtran',
        'advpng_bin'        => 'advpng',

    );
    private $custom_options = array(
        'optipng_options'   => array('quality' => ''),
        'pngquant_options'  => array('quality' => ''),
        'pngcrush_options'  => array('quality' => ''),
        'pngout_options'    => array('quality' => ''),
        'gifsicle_options'  => array('quality' => ''),
        'jpegoptim_options' => array('quality' => ''),
        'jpegtran_options'  => array('quality' => ''),
        'advpng_options'    => array('quality' => ''),

    );
    private $executableFinder;
    private $logger;

    public function __construct(array $options = array(), $logger = null)
    {
        $this->executableFinder = new optimizeExecFinder();
        $this->logger = $logger ?: new optimizeLogger();

        $this->setOptions($options);
        $this->setUpOptimizers();
    }

    private function setOptions($options = array())
    {
        foreach ($options as $k => $v) {
            $this->options[$k] = $v;
        }
        if (isset($this->options['quality']) && !empty($this->options['quality'])) {
            $this->setQuantity($this->options['quality']);
        }
    }

    protected function setQuantity($quality)
    {
        if ($quality > 9) {
            $quality = (int)$quality;
            $optipng_ = (int)((7 / 100) * $quality);
            if ($optipng_ < 1) {
                $optipng_ = 1;
            }
            $this->options['optipng_options']['quality'] = '-o' . $optipng_;
            //////////
            $_gu_quantity = $quality;
            if ($quality < 50) {
                $_gu_quantity = 50;
            }
            $_qu_min = (int)($_gu_quantity - 20);
            $_qu_max = (int)$_gu_quantity;
            //$this->options['pngquant_options']['quality'] = '--quality='.$_qu_min.'-'.$_qu_max.'';
            ///////
            $_gif_quantity = $quality;
            if ($quality < 20) {
                $_gif_quantity = 20;
            }
            $_gi = (int)((256 / 100) * $_gif_quantity);
            $this->options['gifsicle_options']['quality'] = '--colors=' . $_gi . '';
            ////////
            $this->options['jpegoptim_options']['quality'] = '-m ' . $quality . '';
        }


    }

    protected function setUpOptimizers()
    {

        $this->optimizers['pngquant'] = $this->wrap(new optimizeImageExec(
            new optimizeExec($this->executable('pngquant'), $this->options['pngquant_options']),
            function ($file_path) {
                $ext = pathinfo($file_path, PATHINFO_EXTENSION);

                return array('--ext=' . ($ext ? '.' . $ext : ''), '--');
            }
        ));
        $this->optimizers['optipng'] = $this->wrap(new optimizeImageExec(
            new optimizeExec($this->executable('optipng'), $this->options['optipng_options'])
        ));
        $this->optimizers['pngcrush'] = $this->wrap(new optimizeImageExec(
            new optimizeExec($this->executable('pngcrush'), $this->options['pngcrush_options'])
        ));
        $this->optimizers['pngout'] = $this->wrap(new optimizeImageExec(
            new optimizeExec($this->executable('pngout'), $this->options['pngout_options'])
        ));
        $this->optimizers['advpng'] = $this->wrap(new optimizeImageExec(
            new optimizeExec($this->executable('advpng'), $this->options['advpng_options'])
        ));
        $this->optimizers['png'] = new optimizeImageChain(array(
            $this->optimizers['pngquant'],
            $this->optimizers['optipng'],
            $this->optimizers['pngcrush'],
            $this->optimizers['advpng'],
        ));

        $this->optimizers['gif'] = $this->optimizers['gifsicle'] = $this->wrap(new optimizeImageExec(
            new optimizeExec($this->executable('gifsicle'), $this->options['gifsicle_options'])
        ));

        $this->optimizers['jpegoptim'] = $this->wrap(new optimizeImageExec(
            new optimizeExec($this->executable('jpegoptim'), $this->options['jpegoptim_options'])
        ));
        $this->optimizers['jpegtran'] = $this->wrap(new optimizeImageExec(
            new optimizeExec($this->executable('jpegtran'), $this->options['jpegtran_options']),
            function ($filepath) {
                return array('-outfile', $filepath);
            }
        ));
        $this->optimizers['jpeg'] = $this->optimizers['jpg'] = new optimizeImageChain(array(
            $this->unwrap($this->optimizers['jpegoptim']),
            $this->unwrap($this->optimizers['jpegtran']),
        ), true);

        $this->optimizers[self::OPTIMIZER_SMART] = $this->wrap(new optimizeImageSmart(array(
            optimizeImageTypeDefine::TYPE_GIF  => $this->optimizers['gif'],
            optimizeImageTypeDefine::TYPE_PNG  => $this->optimizers['png'],
            optimizeImageTypeDefine::TYPE_JPEG => $this->optimizers['jpeg'],
        )));
    }

    private function wrap($optimizer)
    {
        return $this->option('ignore_errors', true) ? new optimizeImageErrorDecorator($optimizer, $this->logger) : $optimizer;
    }

    private function unwrap($optimizer)
    {
        return $optimizer instanceof optimizeImageErrorDecorator ? $optimizer->unwrap() : $optimizer;
    }

    private function executable($name)
    {
        $executableFinder = $this->executableFinder;

        return $this->option($name . '_bin', function () use ($name, $executableFinder) {
            return $executableFinder->find($name, $name);
        });
    }

    private function option($name, $default = null)
    {
        return isset($this->options[$name]) ? $this->options[$name] : $this->resolveDefault($default);
    }

    /**
     * @param string $name
     *
     * @return optimizeImage
     * @throws Exception When requested optimizer does not exist
     */
    public function get($name = self::OPTIMIZER_SMART)
    {
        if (!isset($this->optimizers[$name])) {
            throw new waException(sprintf('Optimizer "%s" not found', $name));
        }

        return $this->optimizers[$name];
    }

    private function resolveDefault($default)
    {
        return is_callable($default) ? call_user_func($default) : $default;
    }
}