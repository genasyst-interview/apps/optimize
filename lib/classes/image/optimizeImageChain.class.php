<?php

class optimizeImageChain implements optimizeImage
{

    private $optimizers;
    private $executeFirst;

    public function __construct($optimizers = array(), $executeFirst = false)
    {
        $this->optimizers = $optimizers;
        $this->executeFirst = (boolean)$executeFirst;
    }

    public function optimize($file_path = '')
    {
        foreach ($this->optimizers as $optimizer) {
            if ($optimizer->optimize($file_path)) {
                return true;
            }
            if ($this->executeFirst) break;
        }

        return false;
    }
}