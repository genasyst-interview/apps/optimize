<?php

class optimizeImageSmart implements optimizeImage
{

    private $optimizers;
    private $typeDefiner;

    public function __construct($optimizers = array(), $typeDefiner = null)
    {
        $this->optimizers = $optimizers;
        $this->typeDefiner = $typeDefiner ?: new optimizeImageTypeDefineSmart();
    }

    public function optimize($file_path = '')
    {
        $type = $this->typeDefiner->guess($file_path);

        if (!isset($this->optimizers[$type])) {
            throw new waException(sprintf('Optimizer for type "%s" not found.', $type));
        }

        return $this->optimizers[$type]->optimize($file_path);
    }
}