<?php

class optimizeImageExec implements optimizeImage
{

    private $exec_obj;
    private $customArgs;

    public function __construct($exec_obj, $extraArgs = null)
    {
        $this->exec_obj = $exec_obj;
        $this->customArgs = $extraArgs;
    }

    public function optimize($file_path = '')
    {
        $customArgs = array($file_path);
        if ($this->customArgs) {
            $customArgs = array_merge(
                is_callable($this->customArgs) ? call_user_func($this->customArgs, $file_path) : $this->customArgs,
                $customArgs
            );
        }
        try {
            $this->exec_obj->execute($customArgs);

            return true;
        } catch (waException $e) {
            waLog::log($e->getMessage(), '/optimize/optimize_image.log');

            return false;
        }
    }
}