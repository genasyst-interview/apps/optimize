<?php

class optimizeImageErrorDecorator implements optimizeImage
{

    private $optimizer;
    private $logger;

    public function __construct($optimizer, $logger)
    {
        $this->optimizer = $optimizer;
        $this->logger = $logger;
    }

    public function optimize($file_path = '')
    {
        try {
            $this->optimizer->optimize($file_path);

            return true;
        } catch (Exception $e) {
            $this->logger->info($e);

            return false;
        }
    }

    public function unwrap()
    {
        return $this->optimizer;
    }
}