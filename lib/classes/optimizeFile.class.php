<?php

class optimizeFile
{
    const ORIGINAL_PREFIX = 'ori_optz';
    protected static $model = null;
    protected $data = array();

    protected function getData($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }

        return null;
    }

    public function getId()
    {
        return $this->getData('id');
    }

    public function __construct($data)
    {

        if (is_array($data) && isset($data['id'])) {
            $this->data = $data;
        } elseif (is_numeric($data) && $data > 0) {
            $file_data = self::getModel()->getById($data);
            if (!empty($file_data)) {
                $this->data = $file_data;
            }

        } elseif (is_string($data) && strpbrk($data, '/')) {

            $data = str_replace(optimizeFiles::getRootPath(), '', $data);
            $file_data = self::getModel()->getByLink($data);

            if (!empty($file_data)) {
                $this->data = $file_data;
            } else {
                $filepath = optimizeFiles::getRootPath() . $data;
                if (file_exists($filepath)) {
                    $path_info = pathinfo($filepath);
                    $filename = $path_info['basename'];
                    $ext = waFiles::extension($filename);
                    $file_data = array(
                        'original_filepath' => '',
                        'filepath'          => $data,
                        'filename'          => $path_info['filename'],
                        'ext'               => $ext,
                        'optimize'          => 0,
                        'app'               => '',
                        'md5'               => md5_file($filepath),
                        'create_datetime'   => date("Y-m-d H:i:s"),
                    );
                    $id = $this->save($file_data);
                    $this->data = self::getModel()->getById($id);
                }
            }
            /* if(!empty($this->data['original_filepath'])) {
                 $file_data = array();
                 $file_data['original_filepath'] = str_replace(optimizeFiles::getRootPath(),'',$this->data['original_filepath']);
                 $this->save($file_data);
             }*/
            if (!empty($this->data['original_filepath'])) {
                $file_data = array();
                $file_data['optimize'] = 1;
                $this->save($file_data);
            }


        } else {
            $this->data = $data;
        }
    }

    public function getFilePath($full = false)
    {
        $filepath = $this->getData('filepath');
        if ($full) {
            $filepath = optimizeFiles::getRootPath() . $filepath;
        }

        return $filepath;
    }

    public function getSize()
    {
        clearstatcache();

        return filesize($this->getFilePath(true));
    }

    public function getFormatSize()
    {
        return optimizeFiles::formatSize($this->getSize(), '%0.2f', 'B,KB,MB,GB');
    }

    public function getOriginalFilePath($full = false)
    {
        $original = $this->getOriginal();
        if ($original) {
            return $original->getFilePath();
        }

        return '';
    }

    public function getOriginalSize()
    {
        $original = $this->getOriginal();
        if ($original) {
            return $original->getSize();
        }

        return '';
    }

    public function getOriginalFormatSize()
    {
        $original = $this->getOriginal();
        if ($original) {
            return $original->getFormatSize();
        }

        return '';
    }

    public function isOptimize()
    {
        return (bool)$this->getData('optimize');
    }

    public function getOriginal()
    {
        if (!empty($this->data['original_filepath'])) {
            $filepath = optimizeFiles::getRootPath() . $this->data['original_filepath'];
            if (file_exists($filepath)) {
                return new optimizeFile($this->data['original_filepath']);
            } else {
                $file_data = array();
                $file_data['original_filepath'] = '';
                $file_data['optimize'] = 0;
                $this->save($file_data);
            }
        }

        return false;
    }

    public function optimize($quality = null, $force = false)
    {

        if ($this->isOptimize()) {
            if (!$force) {
                return true;
            }

            return $this->reOptimize($quality);
        }
        $settings = array();
        if (!is_null($quality)) {
            $settings['quality'] = $quality;
        }
        $data_filepath = $this->getData('filepath');
        if (!optimizeFiles::isOriginal($data_filepath)) {
            $imageOptimizer = optimizeFiles::getImageOptimizer($settings);
            $data_filepath = $this->getData('filepath');
            $filepath = optimizeFiles::getRootPath() . $data_filepath;
            $ext = $this->getData('ext');
            $filename = $this->getData('filename');
            if (!empty($ext) && !empty($filename)) {
                $new_filepath = dirname($filepath) . '/' . $filename . '.' . optimizeFiles::ORIGINAL_PREFIX . '.' . $ext;
                try {
                    $file_data = array();
                    if (!file_exists($new_filepath)) {
                        waFiles::copy($filepath, $new_filepath);
                        $file_data['original_filepath'] = str_replace(optimizeFiles::getRootPath(), '', $new_filepath);
                    }
                    $file_data['original_filepath'] = str_replace(optimizeFiles::getRootPath(), '', $new_filepath);
                    if ($imageOptimizer->optimize($filepath)) {
                        $file_data['optimize'] = 1;
                    }
                    $this->save($file_data);
                    $this->data = self::getModel()->getById($this->getId());
                } catch (waException $e) {
                    waLog::log($e->getMessage(), '/optimize/optimize_file.log');
                }
            }
        }
    }

    public function reOptimize($quality = null)
    {
        $original = $this->getOriginalFilePath(true);
        $filepath = $this->getFilePath(true);
        try {
            waFiles::copy($original, $filepath);
            $file_data = array();
            $file_data['optimize'] = 0;
            $this->save($file_data);
            $this->data = self::getModel()->getById($this->getId());
            $this->optimize($quality);
        } catch (waException $e) {
            waLog::log($e->getMessage(), '/optimize/optimize_file.log');
        }
    }

    public function isOriginal()
    {
        $data_filepath = $this->getData('filepath');
        if (optimizeFiles::isOriginal($data_filepath)) {
            return true;
        }

        return false;
    }

    public function save($data)
    {
        $id = $this->getData('id');
        if (!empty($id)) {
            self::getModel()->updateById($this->getData('id'), $data);
        } else {
            $id = self::getModel()->insert($data);
        }

        return $id;
    }


    public function __toString()
    {
        return (string)$this->data['filepath'];
    }

    protected static function getModel()
    {
        if (self::$model == null) {
            self::$model = new optimizeFileModel();
        }

        return self::$model;
    }
}