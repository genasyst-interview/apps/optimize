<?php


class optimizeImageTypeDefineExtension implements optimizeImageTypeDefine
{
    /**
     * @param string $file_path
     *
     * @return string Image file type, value of one of the TYPE_* const
     */
    public function guess($file_path = '')
    {
        $ext = strtolower(pathinfo($file_path, PATHINFO_EXTENSION));

        switch ($ext) {
            case 'png':
                return self::TYPE_PNG;
            case 'gif':
                return self::TYPE_GIF;
            case 'jpg':
            case 'jpeg':
                return self::TYPE_JPEG;
            default:
                return self::TYPE_UNKNOWN;
        }
    }
}