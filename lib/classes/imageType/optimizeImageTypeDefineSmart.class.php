<?php


class optimizeImageTypeDefineSmart implements optimizeImageTypeDefine
{
    private $typeDefiner;

    public function __construct()
    {
        try {
            $this->typeDefiner = new optimizeImageTypeDefineGd();
        } catch (waException $e) {
            $this->typeDefiner = new optimizeImageTypeDefineExtension();
        }
    }

    public function guess($file_path = '')
    {
        return $this->typeDefiner->guess($file_path);
    }
}