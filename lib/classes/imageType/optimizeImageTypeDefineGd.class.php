<?php


class  optimizeImageTypeDefineGd implements optimizeImageTypeDefine
{
    public function __construct()
    {
        if (!function_exists('gd_info')) {
            throw new waException(sprintf('%s class require gd extension to be enabled', __CLASS__));
        }
    }

    public function guess($file_path = '')
    {
        list(, , $type) = getimagesize($file_path);

        switch ($type) {
            case \IMAGETYPE_PNG:
                return self::TYPE_PNG;
            case \IMAGETYPE_GIF:
                return self::TYPE_GIF;
            case \IMAGETYPE_JPEG:
            case \IMAGETYPE_JPEG2000:
                return self::TYPE_JPEG;
            default:
                return self::TYPE_UNKNOWN;
        }
    }
}