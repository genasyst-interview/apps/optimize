<?php

class optimizeExec
{
    private $query;
    private $args = array();

    public function __construct($query, $args = array())
    {
        $this->query = $query;
        $this->args = $args;
    }

    public function execute($customArgs = array())
    {
        /* if(!is_executable($this->query)) {
             throw new waException(sprintf('Command "%s" not found.', $this->query));
         }*/

        $args = array_merge($this->args, $customArgs);

        $isWindowsPlatform = defined('PHP_WINDOWS_VERSION_BUILD');

        if ($isWindowsPlatform) {
            $suppressOutput = '';
            $escapeShellCmd = 'escapeshellarg';
        } else {
            $suppressOutput = ' 1> /dev/null 2> /dev/null';
            $escapeShellCmd = 'escapeshellcmd';
        }
        $command = $escapeShellCmd($this->query) . ' ' . implode(' ', array_map('escapeshellarg', $args)) . $suppressOutput;
        exec($command, $output, $result);
        /*  var_dump($command).PHP_EOL;
          var_dump($output).PHP_EOL;
          var_dump($result).PHP_EOL;*/
        waLog::log($command, '/optimize/optimize_image.log');
        if ($result == 127) {
            throw new waException(sprintf('Command "%s" not found.', $command));
        } else if ($result != 0) {
            throw new waException(sprintf('Command failed, return code: %d, command: %s', $result, $command));
        }

        return true;
    }
}