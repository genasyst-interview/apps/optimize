<?php

class optimizeFiles extends waFiles
{
    const ORIGINAL_PREFIX = 'ori_optz';
    protected static $image_optimizer = null;
    protected static $list_dir_file_statuses = array(
        'optimized'    => false,
        'no_optimized' => false,
    );
    protected static $file_types = array(
        'image' => array(
            'jpg'  => true,
            'jpeg' => true,
            'png'  => true,
            'gif'  => true,
        ),
    );

    public static function getRootPath()
    {
        return wa()->getConfig()->getRootPath() . '/';
    }

    public static function getDataPath($app, $path)
    {
        return wa()->getDataPath($path, true, $app, true);
    }

    public static function setListDirSetings($settings)
    {
        if (isset($settings['file_statuses'])) {
            $file_statuses = $settings['file_statuses'];
        }
        if (isset($settings['file_types']) && is_array($settings['file_types'])) {
            foreach ($settings['file_types'] as $type => $exts) {
                if (isset(self::$file_types[$type]) && is_array($exts)) {
                    if (isset($exts['jpg'])) {
                        $exts['jpeg'] = $exts['jpg'];
                    }
                    foreach (self::$file_types[$type] as $ext => $status) {
                        $status = false;
                        if (array_key_exists($ext, $exts)) {
                            $status = $exts[$ext];
                        }
                        if ($status > 0) {
                            self::$file_types[$type][$ext] = true;
                        } else {
                            self::$file_types[$type][$ext] = false;
                        }
                    }
                }
            }
            $file_statuses = $settings['file_statuses'];
        }
        self::$list_dir_file_statuses = $file_statuses;
    }

    public static function listdir($dir, $recursive = true)
    {
        if (!file_exists($dir) || !is_dir($dir)) {
            return array();
        }
        if (!($dh = opendir($dir))) {
            return array();
        }

        $result = array();
        while (false !== ($file = readdir($dh))) {
            if ($file == '.' || $file == '..') {
                continue;
            }

            if ($recursive && is_dir($dir . '/' . $file)) {
                $files = parent::listdir($dir . '/' . $file, $recursive);
                foreach ($files as $sub_file) {
                    $filepath = rtrim($dir, '\/') . '/' . $file . '/' . $sub_file;
                    if (self::checkIncludeFile($filepath)) {
                        $_file = new optimizeFile($filepath);
                        $file_optimize = $_file->isOptimize();
                        if (self::$list_dir_file_statuses['optimized'] && $file_optimize) {
                            $result[] = $_file;
                        } elseif (self::$list_dir_file_statuses['no_optimized'] && !$file_optimize) {
                            $result[] = $_file;
                        }
                    }
                }
            } else {
                $filepath = rtrim($dir, '\/') . '/' . $file;
                if (self::checkIncludeFile($filepath)) {
                    $_file = new optimizeFile($filepath);
                    $file_optimize = $_file->isOptimize();
                    if (self::$list_dir_file_statuses['optimized'] && $file_optimize) {
                        $result[] = $_file;
                    } elseif (self::$list_dir_file_statuses['no_optimized'] && !$file_optimize) {
                        $result[] = $_file;
                    }
                }
            }
        }
        closedir($dh);

        return $result;
    }

    protected static function checkIncludeFile($filepath)
    {
        $ext = self::extension($filepath);
        if (!self::isOriginal($filepath) && array_key_exists($ext, self::$file_types['image']) && self::$file_types['image'][$ext] == true) {
            return true;
        }

        return false;
    }

    public static function isOriginal($filepath)
    {
        if (preg_match('/\.' . self::ORIGINAL_PREFIX . '\./', $filepath)) {
            return true;
        }

        return false;
    }

    public static function getImageOptimizer($settings = array())
    {
        ksort($settings);
        if (!is_array($settings)) {
            $settings = array();
        }
        $md5 = md5(serialize($settings));
        if (self::$image_optimizer[$md5] == null) {
            $settings['ignore_errors'] = false;

            $optimizerImageFactory = new optimizeImageFactory($settings);
            self::$image_optimizer[$md5] = $optimizerImageFactory->get();
        }

        return self::$image_optimizer[$md5];
    }
}