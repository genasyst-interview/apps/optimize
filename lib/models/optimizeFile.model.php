<?php

class optimizeFileModel extends waModel
{
    protected $table = 'optimize_file';

    public function getByLink($filepath)
    {
        return $this->getByField('filepath', $filepath);
    }
}