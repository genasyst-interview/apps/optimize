

function var_dump () {
    var output = '', pad_char = ' ', pad_val = 4, lgth = 0, i = 0, d = this.window.document;
    var getFuncName = function (fn) {
        var name = (/\W*function\s+([\w\$]+)\s*\(/).exec(fn);
        if (!name) {
            return '(Anonymous)';
        }
        return name[1];
    };
    var repeat_char = function (len, pad_char) {
        var str = '';
        for (var i=0; i < len; i++) {
            str += pad_char;
        }
        return str;
    };
    var getScalarVal = function (val) {
        var ret = '';
        if (val === null) {
            ret = 'NULL';
        } else if (typeof val === 'boolean') {
            ret = 'bool(' + val + ')';
        } else if (typeof val === 'string') {
            ret = 'string(' + val.length + ') "' + val + '"';
        } else if (typeof val === 'number') {
            if (parseFloat(val) == parseInt(val, 10)) {
                ret = 'int(' + val + ')';
            } else {
                ret = 'float(' + val + ')';
            }
        } else if (val === undefined) {
            ret = 'UNDEFINED'; // Not PHP behavior, but neither is undefined as value
        }  else if (typeof val === 'function') {
            ret = 'FUNCTION'; // Not PHP behavior, but neither is function as value
            ret = val.toString().split("\n");
            txt = '';
            for(var j in ret) {
                txt += (j !=0 ? thick_pad : '') + ret[j] + "\n";
            }
            ret = txt;
        } else if (val instanceof Date) {
            val = val.toString();
            ret = 'string('+val.length+') "' + val + '"'
        }
        else if(val.nodeName) {
            ret = 'HTMLElement("' + val.nodeName.toLowerCase() + '")';
        }
        return ret;
    };
    var formatArray = function (obj, cur_depth, pad_val, pad_char) {
        var someProp = '';
        if (cur_depth > 0) {
            cur_depth++;
        }
        base_pad = repeat_char(pad_val * (cur_depth - 1), pad_char);
        thick_pad = repeat_char(pad_val * (cur_depth + 1), pad_char);
        var str = '';
        var val = '';
        if (typeof obj === 'object' && obj !== null) {
            if (obj.constructor && getFuncName(obj.constructor) === 'PHPJS_Resource') {
                return obj.var_dump();
            }
            lgth = 0;
            for (someProp in obj) {
                lgth++;
            }
            str += "array(" + lgth + ") {\n";
            for (var key in obj) {
                if (typeof obj[key] === 'object' && obj[key] !== null && !(obj[key] instanceof Date) && !obj[key].nodeName) {
                    str += thick_pad + "["+key+"] =>\n" + thick_pad+formatArray(obj[key], cur_depth+1, pad_val, pad_char);
                } else {
                    val = getScalarVal(obj[key]);
                    str += thick_pad + "["+key+"] =>\n" + thick_pad + val + "\n";
                }
            }
            str += base_pad + "}\n";
        } else {
            str = getScalarVal(obj);
        }
        return str;
    };
    output = formatArray(arguments[0], 0, pad_val, pad_char);
    for ( i=1; i < arguments.length; i++ ) {
        output += '\n' + formatArray(arguments[i], 0, pad_val, pad_char);
    }
    return output;
}
function av(data){
    alert(var_dump(data));
}

function waNet() {
    this.requests = {};
    this.requests_work = 0;
    this.requests_max = 2;
    this.current_request_id = 0;
    this.requests_count = 0;

    this.delay = 200;
    this.post_type = 'json';
    this.method = 'post';
    this.status = 'stop';
}
waNet.prototype.setRequests = function(requests) {

    for(var k in requests) {
        this.requests[this.requests_count] = requests[k];
        this.requests_count++;
    }
};
waNet.prototype.setMethod = function (method) {
    if(method=='get') {
        this.method = 'get';
    } else if (method == 'post') {
        this.method = 'post';
    }
};
waNet.prototype.start = function() {
        this.status = 'work';
        this.recursiveRequest();
};
waNet.prototype.recursiveRequest = function() {
    if(this.status == 'work') {
        if(this.requests_work < this.requests_max) {
            if(this.current_request_id < this.requests_count) {
                this.request(this.requests[this.current_request_id]);
                this.current_request_id++;
            } else {
                this.status = 'complete';
            }
        } else {
            var self = this;
            setTimeout(function(){self.recursiveRequest()},this.delay);
        }
    }
};
waNet.prototype.request = function(request) {
        this.requests_work++;
        var self = this;
        var response = function (response) {
            self.requests_work--;
            request.response(response);
        };
        if(this.method == 'get') {
            this.requestGet(request, response);
        } else if(this.method == 'post') {
            this.requestPost(request, response);
        }
    setTimeout(function(){self.recursiveRequest()},this.delay);

};
waNet.prototype.requestGet = function(request, response) {
    $.get(request.url,request.data,response);
};
waNet.prototype.requestPost = function(request, response) {
    $.post(request.url, request.data, response, this.post_type);
};
waNet.prototype.firstRequest = function (request) {
    
};
(function ($) {
    $.optimize = {
        filesOptimize: function(form) {
            var files = form.find('input[type="checkbox"]:checked');
            var quality = '';
            if(form.find('input[name="quality"]').val()!=='') {
                quality = form.find('input[name="quality"]').val();
            }
            var  reoptimize = 0;
            if(form.find('input[name="reoptimize"]').prop('checked')) {
                reoptimize = 1;
            }
            var requests = {};
            var counter = 0;
            files.each(function () {
               var request = {};
                request['data'] = {'id':$(this).val(),'reoptimize': parseInt(reoptimize), 'quality':quality};
                request['url'] = '?action=file';
                request.response = $.optimize.fileOptimizeResponse;
                requests[counter] = request;
                counter++;
            });
            var _waNet = new waNet();
            _waNet.setRequests(requests);
            _waNet.start();
        },
        fileOptimizeResponse: function (response) {
            if(response.status=='ok') {
                var dt = response.data;
                var file = $('#op-file-'+dt.id);
                file.find('.op-file-name').removeClass('red');
                file.find('.op-file-name').addClass('green');

                file.find('.op-file-name').find('.filename').html(dt.filepath);
                file.find('.op-file-name').find('.op-file-original').html('<br>'+dt.original_filepath+' - '+dt.original_filesize);

                file.find('.op-file-size').removeClass('red');
                file.find('.op-file-size').addClass('green');
                file.find('.op-file-size').html(' <span class=" hint red">'+dt.original_filesize+'</span> -> <span class="green">'+dt.size+'</span>');

                file.find('.op-file-checkbox').attr('checked', false);
            }

        }
    };
})(jQuery);
//Отклики
$(function () {
    $("#files_optimize").submit(function () {
        $.optimize.filesOptimize($(this));
        return false;
    });
    $('.op-file-name').click(function () {
         $(this).closest('.op-file').find('.op-file-original').toggleClass('op-hide');
    });
    $('.select_all').click(function () {
       if($(this).prop('checked')) {
           $(this).closest('form').find('.op-file-checkbox').attr('checked','checked');
       } else {
           $(this).closest('form').find('.op-file-checkbox').attr('checked',false);
       }
    });
    $(window).scroll(function() {
        if($(window).scrollTop() > '200') {
           $('.op-files-save').addClass('fly-button');
        } else {
            $('.op-files-save').removeClass('fly-button');
        }
    });


});